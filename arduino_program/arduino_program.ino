int GO_DELAY_TIME = 100;
int TURN_DELAY_TIME = 100;
int TURN_SPEED = 200;
int GO_SPEED = 150;
int EN1=10; //1-right
int EN2=11;
int IN1=13;
int IN2=12;
int i =0;
int ultrasoundPIN = 6;

void turnleft(int turntime);
void turnright(int turntime);
void turn(int direct);
void go();
void scan();
void carstop();

void setup() {
        // put your setup code here, to run once:
        for(int i=10; i<=13; i++)
        {
                pinMode(i,OUTPUT);
        }
        Serial.begin(9600);
        while(1==1)
        {
                while(Serial.available() == 0)
                {
                        delay(1000);
                }
                String cmd = Serial.readString();
                Serial.println("ACK");
                if(cmd == "SETGO")
                {
                        while(Serial.available() == 0)
                        {
                                delay(1000);
                        }
                        String value = Serial.readString();
                        GO_DELAY_TIME = value.toInt();
                        Serial.println(GO_DELAY_TIME);
                }
                else if(cmd == "SETTURN")
                {
                        while(Serial.available() == 0)
                        {
                                delay(1000);
                        }
                        String value = Serial.readString();
                        TURN_DELAY_TIME = value.toInt();
                        Serial.println(TURN_DELAY_TIME);
                }
                else if(cmd == "SETGOSPEED")
                {
                        while(Serial.available() == 0)
                        {
                                delay(1000);
                        }
                        String value = Serial.readString();
                        GO_SPEED = value.toInt();
                        Serial.println(GO_SPEED);
                }
                else if(cmd == "SETTURNSPEED")
                {
                        while(Serial.available() == 0)
                        {
                                delay(1000);
                        }
                        String value = Serial.readString();
                        TURN_SPEED = value.toInt();
                        Serial.println(TURN_SPEED);
                        break;
                }
        }
}

void loop() {
        while(Serial.available()>0) {
                int cmd = Serial.read();
                // Serial.println(cmd);
                Serial.println("ACK");
                delay(500);
                switch(cmd) {
                    case 'G':
                            go();
                            break;
                    case 'S':
                            scan();
                            break;
                    case 'T':
                            while(Serial.available() == 0)
                            {
                                    delay(1000);
                            }
                            String value = Serial.readString();
                            int direct = value.toInt();
                            turn(direct);
                            break;
                }
        }
}

void turnleft(int turntime)
{
        analogWrite(EN1,TURN_SPEED);
        analogWrite(EN2,0);
        digitalWrite(IN1,LOW);
        digitalWrite(IN2,HIGH);
        delay(TURN_DELAY_TIME * turntime);
        carstop();
}

void turnright(int turntime)
{
        analogWrite(EN1,0);
        analogWrite(EN2,TURN_SPEED);
        digitalWrite(IN1,HIGH);
        digitalWrite(IN2,LOW);
        delay(TURN_DELAY_TIME * turntime);
        carstop();
}

void turn(int direct)
{
        int turntimes = abs(direct);
        if(direct<0)
        {
                turnright(turntimes);
        }
        else
        {
                turnleft(turntimes);
        }
        Serial.println("TURN OK");
}

void go()
{
        analogWrite(EN1,GO_SPEED);
        analogWrite(EN2,GO_SPEED);
        digitalWrite(IN1,HIGH);
        digitalWrite(IN2,HIGH);
        delay(GO_DELAY_TIME);
        carstop();
        Serial.println("GO OK");
}

void scan()
{
        float duration, distance;
        pinMode (ultrasoundPIN, OUTPUT);
        digitalWrite(ultrasoundPIN, LOW);
        delayMicroseconds(2);
        digitalWrite(ultrasoundPIN, HIGH);
        delayMicroseconds(5);
        digitalWrite(ultrasoundPIN, LOW);
        pinMode (ultrasoundPIN, INPUT);
        duration = pulseIn (ultrasoundPIN, HIGH);
        distance = (duration/2)/29;
        Serial.println(distance);
}

void carstop()
{
        analogWrite(EN1,0);
        analogWrite(EN2,0);
}
