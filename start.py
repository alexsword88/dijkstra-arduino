import socket
from datetime import datetime
import copy

SERVER_IP = '192.168.4.1'
BUFFER_SIZE = 1024
SOCKET_PORT = 9000


class XY:
    def __init__(self, x, y, path=[]):
        """XY Point constructor."""
        self.x = x
        self.y = y
        self.path = copy.deepcopy(path)

    def __len__(self):
        """Return path length."""
        return len(self.path)

    def __eq__(self, other):
        """Point compare."""
        return self.x == other.x and self.y == other.y

    def __lt__(self, other):
        """Path compare."""
        return len(self.path) < len(other.path)

    def __sub__(self, other):
        """Subtract X and Y."""
        return (self.x - other.x, self.y - other.y)

    def __str__(self):
        """Point Pretty Print."""
        return "({}, {})".format(self.x, self.y)

    def point(self):
        return XY(self.x, self.y)

    def replace(self, x, y):
        self.x = x
        self.y = y


WIDTH, HEIGHT = (3, 4)
MAX = WIDTH * HEIGHT * 2
GO_DELAY = 2609
TURN_DELAY = 1700
GO_SPEED = 150
TURN_SPEED = 200
OBJECT_DISTANCE = 35
map_data = []
object = []
check_q = []
carposition = XY(1, 3)
goal = XY(2, 0)
goal_ava = []
head_position_conv = {"y1": 0, "x-1": 1, "y-1": 2, "x1": 3}
head_current = 0


def init_array():
    global map_data, WIDTH, HEIGHT, carposition, goal, MAX
    map_data = [[MAX]*WIDTH for i in range(HEIGHT)]
    map_data[carposition.y][carposition.x] = 0
    map_data[goal.y][goal.x] = -1


def same_check(point):
    global goal_ava
    try:
        goal_ava.index(point)
    except ValueError:
        return False
    return True


def object_check(pointx, pointy):
    global object
    try:
        object.index(XY(pointx, pointy))
    except ValueError:
        return False
    return True


def push_queue(pointnew, pointnow):
    global map_data, check_q
    pointnow_weight = map_data[pointnow.y][pointnow.x]
    pointnew_weight = map_data[pointnew.y][pointnew.x]
    if pointnew_weight == -1:
        if not same_check(pointnow):
            temp = XY(pointnow.x, pointnow.y)
            if len(pointnow) > 0:
                temp.path = copy.deepcopy(pointnow.path)
            temp.path.append(pointnow.point())
            goal_ava.append(temp)
        return -1
    elif pointnow_weight+1 > pointnew_weight:
        return pointnew_weight
    else:
        temp = XY(pointnew.x, pointnew.y)
        if len(pointnow) > 0:
            temp.path = copy.deepcopy(pointnow.path)
        temp.path.append(pointnow.point())
        check_q.append(temp)
        return pointnow_weight+1


def turn_optimal(turn_direction):
    if turn_direction == -3:
        return 1
    elif turn_direction == 3:
        return -1
    else:
        return turn_direction


def queue_optimal():
    global check_q
    temp_q = []
    while len(check_q) > 0:
        temp_data = check_q.pop(0)
        try:
            same_data_index = check_q.index(temp_data)
            if len(check_q[same_data_index]) != len(temp_data):
                temp_q.append(temp_data)
        except ValueError:
            temp_q.append(temp_data)
    check_q = copy.deepcopy(temp_q)


def scan(checkingpoint):
    global head_current, object, OBJECT_DISTANCE
    scan_distance = []
    four_points = [XY(checkingpoint.x, checkingpoint.y-1),
                   XY(checkingpoint.x+1, checkingpoint.y),
                   XY(checkingpoint.x, checkingpoint.y+1),
                   XY(checkingpoint.x-1, checkingpoint.y)]
    for i in range(0, 4):
        turn_direction = turn_optimal(head_current - i)
        com_robot("T", str(turn_direction))
        head_current = i
        scan_distance.append(True
                             if float(com_robot("S", None)) > OBJECT_DISTANCE
                             else False)
        if not scan_distance[-1]:
            object.append(four_points[i])
    return scan_distance


def check_neigh(pointnow):
    global WIDTH, HEIGHT, map_data, OBJECT_DISTANCE
    temp = XY(-1, -1)
    accept_point = scan(pointnow)
    if pointnow.y - 1 >= 0 and not object_check(pointnow.x, pointnow.y-1):
        temp.replace(pointnow.x, pointnow.y - 1)
        if accept_point[0]:
            map_data[temp.y][temp.x] = push_queue(temp, pointnow)
    if pointnow.x + 1 < WIDTH and not object_check(pointnow.x+1, pointnow.y):
        temp.replace(pointnow.x + 1, pointnow.y)
        if accept_point[1]:
            map_data[temp.y][temp.x] = push_queue(temp, pointnow)
    if pointnow.y + 1 < HEIGHT and not object_check(pointnow.x, pointnow.y+1):
        temp.replace(pointnow.x, pointnow.y + 1)
        if accept_point[2]:
            map_data[temp.y][temp.x] = push_queue(temp, pointnow)
    if pointnow.x - 1 >= 0 and not object_check(pointnow.x-1, pointnow.y):
        temp.replace(pointnow.x - 1, pointnow.y)
        if accept_point[3]:
            map_data[temp.y][temp.x] = push_queue(temp, pointnow)


def toCheckpoint(pointnow):
    global head_position_conv, head_current
    paths = copy.deepcopy(pointnow.path)
    paths.append(pointnow.point())
    print(paths)
    for index in range(1, len(paths)):
        diff = paths[index-1] - paths[index]
        if diff[0] != 0:
            turn_direction = (head_current -
                              head_position_conv["x{}".format(diff[0])])
        else:
            turn_direction = (head_current -
                              head_position_conv["y{}".format(diff[1])])
        head_current = head_current - turn_direction
        turn_direction = turn_optimal(turn_direction)
        com_robot("T", str(turn_direction))
        com_robot("G", None)


def backtostart(pointnow):
    global head_position_conv, head_current
    paths = copy.deepcopy(pointnow.path)
    paths.append(pointnow.point())
    paths = paths[::-1]

    for index in range(1, len(paths)):
        diff = paths[index-1] - paths[index]
        if diff[0] != 0:
            turn_direction = (head_current -
                              head_position_conv["x{}".format(diff[0])])
        else:
            turn_direction = (head_current -
                              head_position_conv["y{}".format(diff[1])])
        head_current = head_current - turn_direction
        turn_direction = turn_optimal(turn_direction)
        com_robot("T", str(turn_direction))
        com_robot("G", None)


def map_print():
    global map_data, MAX
    for row in map_data:
        print("| ", end="")
        for data in row:
            print("{0:^{width}}".format(data, width=len(str(MAX))+2), end="")
        print("|")
    print("")


def path_print():
    global goal_ava
    goal_ava = sorted(goal_ava)
    for index, paths in enumerate(goal_ava):
        print("Path {}: ".format(index+1), end="")
        print(paths.path[0], end="")
        for path in paths.path[1:]:
            print(" -> {}".format(path), end="")
        print("")


def q_path_print():
    global check_q
    for index, paths in enumerate(check_q):
        print("Path {}: ".format(index+1), end="")
        print(paths.path[0], end="")
        for path in paths.path[1:]:
            print(" -> {}".format(path), end="")
        print(" -> {}".format(paths), end="")
        print("")
    print("")


def wait_ack():
    global robot
    while True:
        data = robot.recv(BUFFER_SIZE)
        if data.decode().strip() == "ACK":
            break


def com_robot(event, value):
    global robot
    data = None
    print(event, value)
    robot.send(event.encode("utf-8"))
    wait_ack()
    if value:
        robot.send(value.encode("utf-8"))
    data = robot.recv(BUFFER_SIZE)
    data = data.decode().strip()
    return data


if __name__ == '__main__':
    robot = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    robot.connect((SERVER_IP, SOCKET_PORT))

    timestart = datetime.now()

    print(com_robot("SETGO", str(GO_DELAY)))
    print(com_robot("SETTURN", str(TURN_DELAY)))
    print(com_robot("SETGOSPEED", str(GO_SPEED)))
    print(com_robot("SETTURNSPEED", str(TURN_SPEED)))

    init_array()
    check_neigh(carposition)
    print(len(check_q))
    map_print()
    q_path_print()
    while len(check_q) > 0:
        current_point = check_q.pop(0)
        toCheckpoint(current_point)
        check_neigh(current_point)
        backtostart(current_point)
        queue_optimal()
        print(len(check_q))
        map_print()
        q_path_print()
    path_print()
    toCheckpoint(sorted(goal_ava)[0])
    robot.close()
    timeend = datetime.now()
    print("From {} to {}".format(str(timestart), str(timeend)))
    print("Total time: {}".format(str(timeend - timestart)))
