from datetime import datetime
import copy


class XY:
    def __init__(self, x, y, path=[]):
        """XY Point constructor."""
        self.x = x
        self.y = y
        self.path = copy.deepcopy(path)

    def __len__(self):
        """Return path length."""
        return len(self.path)

    def __eq__(self, other):
        """Point compare."""
        return self.x == other.x and self.y == other.y

    def __lt__(self, other):
        """Path compare."""
        return len(self.path) < len(other.path)

    def __sub__(self, other):
        """Subtract X and Y."""
        return (self.x - other.x, self.y - other.y)

    def __str__(self):
        """Point Pretty Print."""
        return "({}, {})".format(self.x, self.y)

    def point(self):
        return XY(self.x, self.y)

    def replace(self, x, y):
        self.x = x
        self.y = y


WIDTH, HEIGHT = (3, 4)
MAX = WIDTH * HEIGHT * 2
map_data = []
object = [XY(0, 2), XY(0, 0), XY(2, 1)]
check_q = []
carposition = XY(1, 3)
goal = XY(2, 0)
goal_ava = []
head_position_conv = {"y1": 0, "x-1": 1, "y-1": 2, "x1": 3}
head_current = 0


def init_array():
    global map_data, WIDTH, HEIGHT, carposition, goal, MAX
    map_data = [[MAX]*WIDTH for i in range(HEIGHT)]
    map_data[carposition.y][carposition.x] = 0
    map_data[goal.y][goal.x] = -1


def same_check(point):
    global goal_ava
    try:
        goal_ava.index(point)
    except ValueError:
        return False
    return True


def object_check(pointx, pointy):
    global object
    try:
        object.index(XY(pointx, pointy))
    except ValueError:
        return False
    return True


def push_queue(pointnew, pointnow):
    global map_data, check_q
    pointnow_weight = map_data[pointnow.y][pointnow.x]
    pointnew_weight = map_data[pointnew.y][pointnew.x]
    if pointnew_weight == -1:
        if not same_check(pointnow):
            temp = XY(pointnow.x, pointnow.y)
            if len(pointnow) > 0:
                temp.path = copy.deepcopy(pointnow.path)
            temp.path.append(pointnow.point())
            goal_ava.append(temp)
        return -1
    elif pointnow_weight+1 > pointnew_weight:
        return pointnew_weight
    else:
        temp = XY(pointnew.x, pointnew.y)
        if len(pointnow) > 0:
            temp.path = copy.deepcopy(pointnow.path)
        temp.path.append(pointnow.point())
        check_q.append(temp)
        return pointnow_weight+1


def turn_optimal(turn_direction):
    if turn_direction == -3:
        return 1
    elif turn_direction == 3:
        return -1
    else:
        return turn_direction


def queue_optimal():
    global check_q
    temp_q = []
    while len(check_q) > 0:
        temp_data = check_q.pop(0)
        try:
            same_data_index = check_q.index(temp_data)
            if len(check_q[same_data_index]) != len(temp_data):
                temp_q.append(temp_data)
        except ValueError:
            temp_q.append(temp_data)
    check_q = copy.deepcopy(temp_q)


def check_neigh(pointnow):
    global WIDTH, HEIGHT, map_data
    temp = XY(-1, -1)
    if pointnow.y - 1 >= 0 and not object_check(pointnow.x, pointnow.y-1):
        temp.replace(pointnow.x, pointnow.y - 1)
        map_data[temp.y][temp.x] = push_queue(temp, pointnow)
    if pointnow.x + 1 < WIDTH and not object_check(pointnow.x+1, pointnow.y):
        temp.replace(pointnow.x + 1, pointnow.y)
        map_data[temp.y][temp.x] = push_queue(temp, pointnow)
    if pointnow.y + 1 < HEIGHT and not object_check(pointnow.x, pointnow.y+1):
        temp.replace(pointnow.x, pointnow.y + 1)
        map_data[temp.y][temp.x] = push_queue(temp, pointnow)
    if pointnow.x - 1 >= 0 and not object_check(pointnow.x-1, pointnow.y):
        temp.replace(pointnow.x - 1, pointnow.y)
        map_data[temp.y][temp.x] = push_queue(temp, pointnow)


def map_print():
    global map_data, MAX
    for row in map_data:
        print("| ", end="")
        for data in row:
            print("{0:^{width}}".format(data, width=len(str(MAX))+2), end="")
        print("|")
    print("")


def path_print():
    global goal_ava
    goal_ava = sorted(goal_ava)
    for index, paths in enumerate(goal_ava):
        print("Path {}: ".format(index+1), end="")
        print(paths.path[0], end="")
        for path in paths.path[1:]:
            print(" -> {}".format(path), end="")
        print("")


def q_path_print():
    global check_q
    for index, paths in enumerate(check_q):
        print("Path {}: ".format(index+1), end="")
        print(paths.path[0], end="")
        for path in paths.path[1:]:
            print(" -> {}".format(path), end="")
        print(" -> {}".format(paths), end="")
        print("")
    print("")


if __name__ == '__main__':
    timestart = datetime.now()

    init_array()
    check_neigh(carposition)
    print(len(check_q))
    map_print()
    q_path_print()
    while len(check_q) > 0:
        current_point = check_q.pop(0)
        check_neigh(current_point)
        queue_optimal()
        print(len(check_q))
        map_print()
        q_path_print()
    path_print()

    timeend = datetime.now()
    print("From {} to {}".format(str(timestart), str(timeend)))
    print("Total time: {}".format(str(timeend - timestart)))
